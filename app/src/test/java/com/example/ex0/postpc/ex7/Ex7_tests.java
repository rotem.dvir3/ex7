package com.example.ex0.postpc.ex7;

import android.content.Context;
import android.content.Intent;

import androidx.test.platform.app.InstrumentationRegistry;

import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.annotation.LooperMode;


import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.eq;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 28)
@LooperMode(LooperMode.Mode.PAUSED)
public class Ex7_tests extends TestCase {

    private OrderSaverApp mockDataBase;
    private FirebaseFirestore db;
    private Order cur_item;

    //    @Test
//    public void Check_App_Navigate() {
//        // Context of the app under test.
//        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
//
//        FirebaseApp.initializeApp(InstrumentationRegistry.getInstrumentation().getTargetContext());
//        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        Order cur_order = new Order();
//        cur_order.Status="in-progress";
//
//        db.collection("Orders").document(cur_order.ID).set(cur_order);
//
//        assertEquals(InProgressActivity.class, appContext.getClass());
//    }
//
//
//    @Test
//    public void Check_App_Navigate_2() {
//        // Context of the app under test.
//        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
//
//        FirebaseApp.initializeApp(InstrumentationRegistry.getInstrumentation().getTargetContext());
//        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        Order cur_order = new Order();
//        cur_order.Status="in-progress";
//        db.collection("Orders").document(cur_order.ID).set(cur_order);
//        final Intent intent = appContext.getPackageManager()
//                .getLaunchIntentForPackage();
//        // Clear out any previous instances
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        appContext.startActivity(intent);
//
//        assertEquals(InProgressActivity.class, appContext.getClass());
//    }
    @Before
    public void setup() {
        mockDataBase =OrderSaverApp.getInstance();
        FirebaseApp.initializeApp(InstrumentationRegistry.getInstrumentation().getTargetContext());
        db = FirebaseFirestore.getInstance();
        cur_item= new Order();
        cur_item.Comment="hello";
        db.collection("Orders").document(cur_item.ID).set(cur_item);
        mockDataBase.getDatabase().add_item(cur_item);





        // let the activity use our `mockDataBase` as the TodoItemsDataBase
    }


    @Test
    public void when_edit_screnn_Tahini_change() {
        // setup

        // when asking the `mockDataBase` to get the current items, return an empty list



        ActivityController<EditOrderActivity> activityController = Robolectric.buildActivity(EditOrderActivity.class);

        // let the activity use our `mockDataBase` as the TodoItemsDataBase
        EditOrderActivity activityUnderTest = activityController.get();
        activityUnderTest.holder = mockDataBase.getDatabase();

//        activityController.create().visible();
        // Clear out any previous instances
//        RadioButton r_but = activityUnderTest.findViewById(R.id.yes_tahini);
//        r_but.performClick();
        activityUnderTest.holder.cur_items.Tahini=true;

        // verify
        assertTrue(mockDataBase.getDatabase().cur_items.Tahini);
        mockDataBase.getDatabase().cur_items.Tahini=false;


    }

    @Test
    public void when_EditScrenn_then_thanEdit() {

        // setup

        // when asking the `mockDataBase` to get the current items, return an empty list



        ActivityController<EditOrderActivity> activityController = Robolectric.buildActivity(EditOrderActivity.class);

        // let the activity use our `mockDataBase` as the TodoItemsDataBase
        EditOrderActivity activityUnderTest = activityController.get();
        activityUnderTest.holder = mockDataBase.getDatabase();

        activityController.create().visible();


        // Clear out any previous instances
        EditText editText = activityUnderTest.findViewById(R.id.editTextInsertTask);
        String userInput = editText.getText().toString();
        // verify
        assertEquals("hello", userInput);

    }

    @Test
    public void when_EditScrenn_then_thanDeleteingWorks() {
        // setup
        // when asking the `mockDataBase` to get the current items, return an empty list

        ActivityController<EditOrderActivity> activityController = Robolectric.buildActivity(EditOrderActivity.class);

        // let the activity use our `mockDataBase` as the TodoItemsDataBase
        EditOrderActivity activityUnderTest = activityController.get();
        activityUnderTest.holder = mockDataBase.getDatabase();
        activityController.create().visible();
        View fab = activityUnderTest.findViewById(R.id.buttonDelete);
        fab.performClick();
        assertNull(mockDataBase.getDatabase().get_Id());

    }


}