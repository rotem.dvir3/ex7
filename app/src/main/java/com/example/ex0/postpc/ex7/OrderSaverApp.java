package com.example.ex0.postpc.ex7;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;

    public class OrderSaverApp extends Application {

        public dataHolder getDatabase() {
            return database;
        }

        public static OrderSaverApp getInstance() {
            return instance;
        }

        private static OrderSaverApp instance =null;

        private static dataHolder database;
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onCreate() {
            super.onCreate();
            if (database==null){
                database= new dataHolder(this);


            }
            instance=this;
        }



    }


