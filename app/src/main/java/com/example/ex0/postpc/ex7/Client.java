package com.example.ex0.postpc.ex7;

import java.util.ArrayList;
import java.util.List;

public class Client {
    int client_id;
    ArrayList<Order> orders = null;
    public Client(int id){
        client_id=id;
        orders= new ArrayList<>();
    }

    public void add_Order(Order cur_order){
        orders.add(cur_order);
    }

    public void remove_order(Order cur_order){
        orders.remove(cur_order);
    }


}
