package com.example.ex0.postpc.ex7;

import java.io.Serializable;
import java.util.UUID;

public class Order implements Serializable {
    public String ID;
    public int Pickles;
    public Boolean Tahini;
    public Boolean Hummus;
    public String Comment;
    public String Status;
    
    public Order(String id,int pickl_num,boolean tahini,boolean hummus,String comment){
        ID=id;
        Pickles=pickl_num;
        Tahini= tahini;
        Hummus= hummus;
        Comment=comment;
        Status="No_order";


    }

    public Order(){
        ID=UUID.randomUUID().toString();
        Pickles=0;
        Tahini= false;
        Hummus= false;
        Comment="";
        Status="No_order";

    }

    public Order(String reps){
        String[] param =reps.split("\\$");
        Pickles=Integer.parseInt(param[1]);
        ID=param[0];
        Tahini = Boolean.parseBoolean(param[2]);
        Hummus = Boolean.parseBoolean(param[3]);
        Comment = param[4];
        Status= param[5];


    }

    public void changeStatus(String status){
        Status=status;
    }

    @Override
    public String toString(){
        return ID + "$" + Pickles + "$" + Tahini + "$" + Hummus + "$" + this.Comment+"$"+Status;
    }

}
