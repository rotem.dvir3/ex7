package com.example.ex0.postpc.ex7;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class dataHolder implements Serializable {
    Order cur_items;
    private static Context myContext =null;
    private static SharedPreferences pref;


    protected dataHolder(Context context){
        cur_items =null;
        myContext=context;
        pref = context.getSharedPreferences("local_db_app",Context.MODE_PRIVATE);


    }





    public void add_item(Order c){
        cur_items= c;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("id",c.ID);
        editor.apply();

    }

    public void delete_item(Order c){
        cur_items=null;
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("id");
        editor.apply();
    }

    public String get_Id(){
        return pref.getString("id",null);
    }


}
