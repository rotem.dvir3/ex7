package com.example.ex0.postpc.ex7;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class DoneActivity extends AppCompatActivity {
    public dataHolder holder = null;
    public Order cur_order=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.done_activity);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (holder == null) {
            // remove and make static
            holder = OrderSaverApp.getInstance().getDatabase();
        }

        cur_order= holder.cur_items;
        cur_order.Status="Ready";
        DocumentReference docRef = db.collection("Orders").document(cur_order.ID);

        Button confirm = findViewById(R.id.confirm_button);
        confirm.setOnClickListener(v->{
                holder.delete_item(cur_order);
                docRef.delete()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                private static final String TAG ="delete" ;

                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                    Intent l = new Intent(DoneActivity.this,MainActivity.class);
                                    startActivity(l);
                                    finish();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                private static final String TAG ="fail to delete" ;

                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error deleting document", e);
                                }
                            });
                //remove order
                // go back to main screen

                }


                );
        // if docref change status move to done


    }}
