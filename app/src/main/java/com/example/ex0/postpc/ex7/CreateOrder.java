package com.example.ex0.postpc.ex7;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.io.Serializable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class CreateOrder extends AppCompatActivity implements AdapterView.OnItemSelectedListener {



    public dataHolder holder = null;
    public Order cur_order=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_order_view);
        if (holder == null) {
            // remove and make static
            holder = OrderSaverApp.getInstance().getDatabase();
        }
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        cur_order= holder.cur_items;
        cur_order.Status="waiting";

        Button finishButton = findViewById(R.id.buttonFinish);
        Spinner Pickle_num = findViewById(R.id.pickle_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.nums, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Pickle_num.setAdapter(adapter);
        Pickle_num.setOnItemSelectedListener(this);
        FloatingActionButton doneEdit = findViewById(R.id.finishEdit);





        EditText commentEdit = findViewById(R.id.editTextInsertTask);
        commentEdit.setText(""); // cleanup text in edit-text
        commentEdit.setEnabled(true); // set edit-text as enabled (user can input text)
        finishButton.setEnabled(true);
        doneEdit.setEnabled(false);
        commentEdit.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                doneEdit.setEnabled(true);


            }
        });
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.choose_tahini);
        RadioGroup radioGroup2 = (RadioGroup)findViewById(R.id.choose_hommus);



        // Uncheck or reset the radio buttons initially
        radioGroup.clearCheck();
        radioGroup2.clearCheck();
        RadioButton yesTahini = findViewById(R.id.yes_tahini);
        RadioButton noTahini = findViewById(R.id.no_tahini);
        noTahini.setOnClickListener(v->{cur_order.Tahini=false;});
        yesTahini.setOnClickListener(v->{
            cur_order.Tahini =true;
        });
        RadioButton yesHummus = findViewById(R.id.yes_hommus);
        RadioButton noHummus = findViewById(R.id.no_hommus);
        noHummus.setOnClickListener(v->{cur_order.Hummus=false;});
        yesHummus.setOnClickListener(v->{
            cur_order.Hummus =true;
        });

        finishButton.setOnClickListener(v -> {
            db.collection("Orders").document(cur_order.ID).set(cur_order);

            Intent toSend = new Intent(v.getContext(),EditOrderActivity.class);
            v.getContext().startActivity(toSend);


        });
        doneEdit.setOnClickListener(v -> {
                    if (commentEdit.getText().toString().length() != 0) {
                        holder.cur_items.Comment=commentEdit.getText().toString();
                        hideKeyboard(this);
                        // make the rcycler show this first;

                    }
                }
        );





}







    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
        String s = editTextUserInput.getText().toString();
        outState.putString("user_last_input", s);
//        outState.putParcelable("layer_man", layoutManager.onSaveInstanceState());
        outState.putSerializable("holder", (Serializable) this.holder);


    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
        editTextUserInput.setText(savedInstanceState.getString("user_last_input"));
//        Parcelable state = savedInstanceState.getParcelable("layer_man");
        holder = (dataHolder) savedInstanceState.getSerializable("holder");
//        layoutManager.onRestoreInstanceState(state);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(cur_order!=null){
            cur_order.Pickles= Integer.parseInt((String)parent.getItemAtPosition(position));

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
