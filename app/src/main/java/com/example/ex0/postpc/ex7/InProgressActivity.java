package com.example.ex0.postpc.ex7;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

public class InProgressActivity  extends AppCompatActivity {
    public dataHolder holder = null;
    public Order cur_order=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inprogrees_activity);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (holder == null) {
            // remove and make static
            holder = OrderSaverApp.getInstance().getDatabase();
        }

        cur_order= holder.cur_items;
        cur_order.Status="in-progress";
        DocumentReference docRef = db.collection("Orders").document(cur_order.ID);

        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            private static final String TAG ="change" ;

            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()&& snapshot.getData().get("Status").equals("Ready") ) {
                    Log.d(TAG, "Current data: " + snapshot.getData());
                    Intent change = new Intent(InProgressActivity.this, DoneActivity.class);
                    startActivity(change);
                    finish();

                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });
        // if docref change status move to done


}}
