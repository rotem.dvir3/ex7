package com.example.ex0.postpc.ex7;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.FirebaseApp;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity{



    public dataHolder holder = null;
    public Order cur_order=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (holder == null) {
            // remove and make static
            holder = OrderSaverApp.getInstance().getDatabase();
        }
        FirebaseApp.initializeApp(this);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(holder.get_Id()!=null){
            DocumentReference docRef = db.collection("Orders").document(holder.get_Id());

            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Order order = documentSnapshot.toObject(Order.class);
                    if (order!=null){
                        holder.add_item(order);

                    }
                    else{
                        holder.add_item(new Order());
                    }
                    Navigate();

                    // go to item

                }
            });





        }
        holder.add_item(new Order());
        Navigate();





    }

    public void Navigate(){
        cur_order =holder.cur_items;

        switch (cur_order.Status){
            case ("No_order"):
                Intent k = new Intent(this, CreateOrder.class);
                finish();
                startActivity(k);
                break;

            case ("waiting"):
                Intent l = new Intent(this, EditOrderActivity.class);
                finish();
                startActivity(l);
                break;

            case ("in-progress"):
                Intent J = new Intent(this,InProgressActivity.class);
                startActivity(J);

                finish();
                break;
            case ("Ready"):
                Intent m = new Intent(this,DoneActivity.class);
                finish();
                startActivity(m);
                break;


        }


    }


    }
