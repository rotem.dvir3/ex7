package com.example.ex0.postpc.ex7;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.content.Intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.io.Serializable;
import java.util.List;
import androidx.lifecycle.Observer;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;


public class EditOrderActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {



    public dataHolder holder = null;
    public Order cur_order=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_activity);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (holder == null) {
            // remove and make static
            holder = OrderSaverApp.getInstance().getDatabase();
        }

        cur_order = holder.cur_items;
        Button deleteButton = findViewById(R.id.buttonDelete);
        Spinner Pickle_num = findViewById(R.id.pickle_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.nums, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Pickle_num.setAdapter(adapter);
        Pickle_num.setOnItemSelectedListener(this);
        FloatingActionButton doneEdit = findViewById(R.id.finishEdit);
        Pickle_num.setSelection(holder.cur_items.Pickles);






        EditText commentEdit = findViewById(R.id.editTextInsertTask);
        commentEdit.setText(holder.cur_items.Comment); // cleanup text in edit-text
        commentEdit.setEnabled(true); // set edit-text as enabled (user can input text)
        doneEdit.setEnabled(false);
        commentEdit.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                doneEdit.setEnabled(true);
                db.collection("Orders").document(cur_order.ID).set(cur_order);


            }
        });
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.choose_tahini);
        RadioGroup radioGroup2 = (RadioGroup)findViewById(R.id.choose_hommus);



        // Uncheck or reset the radio buttons initially
        int tahiniCheck = cur_order.Tahini? 0 : 1;
        int hummusCheck = cur_order.Hummus? 0 : 1;

        radioGroup.check(tahiniCheck);
        radioGroup2.check(hummusCheck);
        RadioButton yesTahini = findViewById(R.id.yes_tahini);
        RadioButton noTahini = findViewById(R.id.no_tahini);
        noTahini.setOnClickListener(v->{cur_order.Tahini=false;
            db.collection("Orders").document(cur_order.ID).set(cur_order);});
        yesTahini.setOnClickListener(v->{
            cur_order.Tahini =true;
            db.collection("Orders").document(cur_order.ID).set(cur_order);
        });
        RadioButton yesHummus = findViewById(R.id.yes_hommus);
        RadioButton noHummus = findViewById(R.id.no_hommus);
        noHummus.setOnClickListener(v->{cur_order.Hummus=false;
            db.collection("Orders").document(cur_order.ID).set(cur_order);
        });
        yesHummus.setOnClickListener(v->{
            cur_order.Hummus =true;
            db.collection("Orders").document(cur_order.ID).set(cur_order);
        });

        deleteButton.setOnClickListener(v -> {
            holder.delete_item(cur_order);
            db.collection("Orders").document(cur_order.ID)
                    .delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("ddd", "DocumentSnapshot successfully deleted!");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("lll", "Error deleting document", e);
                        }
                    });
            Intent l = new Intent(this,MainActivity.class);
            this.finish();
            startActivity(l);


        });
        doneEdit.setOnClickListener(v -> {
                    if (commentEdit.getText().toString().length() != 0) {
                        holder.cur_items.Comment=commentEdit.getText().toString();
                        hideKeyboard(this);
                        db.collection("Orders").document(cur_order.ID).set(cur_order);
                        // make the rcycler show this first;

                    }
                }
        );
        db.collection("Orders").document(cur_order.ID).addSnapshotListener
                (new EventListener<DocumentSnapshot>() {
            private static final String TAG ="change" ;

            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()&& snapshot.getData().get("Status").equals("in-progress") ) {
                    Log.d(TAG, "Current data: " + snapshot.getData());
                    Intent change = new Intent(EditOrderActivity.this,InProgressActivity.class);

                    startActivity(change);
                    finish();

                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });





    }







    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
        String s = editTextUserInput.getText().toString();
        outState.putString("user_last_input", s);
//        outState.putParcelable("layer_man", layoutManager.onSaveInstanceState());
        outState.putSerializable("holder", (Serializable) this.holder);


    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
        editTextUserInput.setText(savedInstanceState.getString("user_last_input"));
//        Parcelable state = savedInstanceState.getParcelable("layer_man");
        holder = (dataHolder) savedInstanceState.getSerializable("holder");
//        layoutManager.onRestoreInstanceState(state);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(cur_order!=null){
            cur_order.Pickles= Integer.parseInt((String)parent.getItemAtPosition(position));

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
